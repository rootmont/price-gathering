import os

user = os.getenv("DBUSER", 'root')
passw = os.getenv("DBPASS", '123456789')
host = os.getenv("DBHOST", 'localhost')
port = os.getenv("DBPORT", 3306)
database = os.getenv("DBDATABASE", 'coin_exchange_db_V2') 