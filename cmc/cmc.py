from bs4 import BeautifulSoup
import requests
import pandas as pd
import logging
import datetime
from sqlalchemy import create_engine
from credentials_db import user, passw, host, port, database
from threading import Thread, Lock
import time
from selenium import webdriver as se
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import random
from time import sleep
import pandas as pd
from bs4 import BeautifulSoup

user = user
passw = passw
host = host
port = port
database = database

mydb = create_engine('mysql+pymysql://' + user + ':' + passw + '@' + host + ':' + str(port) + '/' + database,
                     echo=False)

output = Lock()

threads = []

logging.propagate = False
logger = logging.getLogger(__name__)
logger.propagate = False

"""url = "https://coinmarketcap.com/currencies/ripple/historical-data/?start=20130428&end=20200802"
content = requests.get(url).content
soup = BeautifulSoup(content,'html.parser')
table = soup.find('table', {'class': 'table'})


# In[22]:


data = [[td.text.strip() for td in tr.findChildren('td')]
        for tr in table.findChildren('tr')]
print(data)

# In[23]:


df = pd.DataFrame(data)
df.drop(df.index[0], inplace=True) # first row is empty
df[0] =  pd.to_datetime(df[0]) # date
for i in range(1,7):
    df[i] = pd.to_numeric(df[i].str.replace(",","").str.replace("-","")) # some vol is missing and has -
    print(df[i])
df.columns = ['Date','Open','High','Low','Close','Volume','Market Cap']
df.set_index('Date',inplace=True)
df.sort_index(inplace=True)


# In[24]:


df


# In[25]:


df.to_csv('cmc_data_historical.csv')"""


# In[ ]:
def get_price_marketcap(coin, start_date):
    now = datetime.date.today()
    now = now.strftime("%Y%m%d")
    try:
        url = "https://coinmarketcap.com/currencies/" + coin + "/historical-data/?start=" + start_date + "&end=" + now + ""
        content = requests.get(url).content

        time.sleep(5)
        
        soup = BeautifulSoup(content, 'html.parser')
        table = soup.find('table', {'class': 'table'})

        data = [[td.text.strip() for td in tr.findChildren('td')]
                for tr in table.findChildren('tr')]

        time.sleep(5)

        df = pd.DataFrame(data)
        df.drop(df.index[0], inplace=True)  # first row is empty
        df[0] = pd.to_datetime(df[0])  # date
        for i in range(1, 7):
            df[i] = pd.to_numeric(df[i].str.replace(",", "").str.replace("-", ""))  # some vol is missing and has -
            #print(df[i])
        df.columns = ['Date', 'Open', 'High', 'Low', 'Close', 'Volume', 'Market Cap']
        # df.set_index('Date', inplace=True)
        # df.sort_index(inplace=True)

        df.to_sql(name='cmc_' + '{}'.format(coin), con=mydb, if_exists='replace', index=False, index_label='id')
        
        logger.log("Finished process COIN: {}".format(coin))

        # df.to_csv('cmc_data_historical.csv')
    except Exception as error:

        print(error)
        
        # Create handlers
        f_handler = logging.FileHandler('get_price_marketcap_error.log')

        # Create formatters and add it to handlers
        f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        f_handler.setFormatter(f_format)

        # Add handlers to the logger
        logger.addHandler(f_handler)
        logger.error('COIN5: %s ERROR %s', coin, error)

        pass


# get_price_marketcap('Stellar', '20130428')

def get_list_coins():
    flag = False
    cmc_list_coin = []

    try:
        url = "https://coinmarketcap.com/coins/views/all/"
        content = requests.get(url).content
        soup = BeautifulSoup(content, 'html.parser')
        table = soup.find('table', {'id': 'currencies-all'})

        data = [[td.text.strip() for td in tr.findChildren('td')]
                for tr in table.findChildren('tr')]

        df = pd.DataFrame(data)
        df_exchanges_list = df.values.tolist()
        for i in df_exchanges_list:
            if flag == True:
                word = i[1]
                choise_coin = word.split('\n\n')
                coin = choise_coin[1]
                cmc_list_coin.append(coin)
            else:
                flag = True

        return cmc_list_coin

    except Exception as error:
        # Create handlers
        f_handler = logging.FileHandler('get_price_marketcap_error.log')

        # Create formatters and add it to handlers
        f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        f_handler.setFormatter(f_format)

        # Add handlers to the logger
        logger.addHandler(f_handler)
        logger.error('COIN: %s ERROR %s', word, error)

        print(error)

        pass



def core(start_date):
    now = datetime.date.today()
    now = now.strftime("%Y%m%d")
    cmc_coin_list = get_list_coins()

    random.shuffle(cmc_coin_list)

    initial = 0
    final = 10

    while True:

        threads = []

        segment = cmc_coin_list[initial:final-1]

        print(segment)
        
        initial = final
        final = final + 10

        for coin in segment:
            try:
                print("COIN: Thread Starting: {}".format(coin))
                t = Thread(target=save_coin_marketcap_information, args=(coin, start_date, now))
                threads.append(t)
            except Exception as e:
                print(e)
                continue
        try:
            for x in threads:
                x.start()
            for x in threads:
                x.join()
        
        except Exception as e:
            print(e)
            pass

        time.sleep(60)

        print("Threads: New round: {}-{}".format(initial, final))

        if segment is None:

            break


    while True:

        time.sleep(5)
        
        print("processing...")

def save_coin_marketcap_information(coin, start_date, now):
    try:

        url = "https://coinmarketcap.com/currencies/" + coin + "/historical-data/?start=" + start_date + "&end=" + now + ""
        content = requests.get(url).content

        time.sleep(5)

        soup = BeautifulSoup(content, 'html.parser')
        table = soup.find('table', {'class': 'table'})

        data = [[td.text.strip() for td in tr.findChildren('td')]
                for tr in table.findChildren('tr')]
        
        time.sleep(5)

        df = pd.DataFrame(data)
        df.drop(df.index[0], inplace=True)  # first row is empty
        df[0] = pd.to_datetime(df[0])  # date
        for i in range(1, 7):
            df[i] = pd.to_numeric(df[i].str.replace(",", "").str.replace("-", ""))  # some vol is missing and has -
            # print(df[i])
        df.columns = ['Date', 'Open', 'High', 'Low', 'Close', 'Volume', 'Market Cap']
        # df.set_index('Date', inplace=True)
        # df.sort_index(inplace=True)

        df["supply_open"] = df['Market Cap']/df["Open"] 
        df["supply_high"] = df['Market Cap']/df["High"] 
        df["supply_low"] = df['Market Cap']/df["Low"] 
        df["supply_close"] = df['Market Cap']/df["Close"] 
        
        df.to_sql(name='cmc_' + '{}'.format(coin), con=mydb, if_exists='replace', index=False, index_label='id')
        print("Finished process COIN: {}".format(coin))

    except Exception as error:

        if str(error) == "'NoneType' object has no attribute 'findChildren'":

            print(error)

            get_price_marketcap(coin, '20130428')

        else:

            print(error)

            # Create handlers
            f_handler = logging.FileHandler('save_coin_marketcap_information_error.log')

            # Create formatters and add it to handlers
            f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            f_handler.setFormatter(f_format)

            # Add handlers to the logger
            logger.addHandler(f_handler)
            logger.error('COIN: %s ERROR %s', coin, error)


def save_coin_marketcap_information_using_webdriver(coin, start_date, now):

    try:

        options = se.ChromeOptions()
        options.add_argument('headless')
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument('--no-sandbox')

        driver = se.Chrome('chromedriver',options=options)

        url = "https://coinmarketcap.com/currencies/" + coin + "/historical-data/?start=" + start_date + "&end=" + now + ""
        driver.implicitly_wait(200) # seconds
        driver.get(url)

        print("COIN Starting:{}".format(coin))
        myDynamicElement = driver.find_element_by_id("historical-data")

        #try:
        #    element = WebDriverWait(driver, 100).until(
        #        EC.presence_of_element_located((By.CLASS_NAME, "cmc-main-content__main"))
        #    )
        #finally:
        #    print("COIN: Waiting failed:{}".format(coin))
        #    driver.quit()

        #sleep(60) # give time for all javascripts to be finished running
        print("COIN Finishing:{}".format(coin))
        page0 = driver.page_source

        soup = BeautifulSoup(page0, 'html.parser')
        table = soup.find('table', {'class': 'table'})

        data = [[td.text.strip() for td in tr.findChildren('td')]
                for tr in table.findChildren('tr')]

        df = pd.DataFrame(data)
        df.drop(df.index[0], inplace=True)  # first row is empty
        df[0] = pd.to_datetime(df[0])  # date
        for i in range(1, 7):
            df[i] = pd.to_numeric(df[i].str.replace(",", "").str.replace("-", ""))  # some vol is missing and has -
            #print(df[i])
        df.columns = ['Date', 'Open', 'High', 'Low', 'Close', 'Volume', 'Market Cap']
                
        driver.close()

        df["supply_open"] = df['Market Cap']/df["Open"] 
        df["supply_high"] = df['Market Cap']/df["High"] 
        df["supply_low"] = df['Market Cap']/df["Low"] 
        df["supply_close"] = df['Market Cap']/df["Close"] 
        
        df.to_sql(name='cmc_' + '{}'.format(coin), con=mydb, if_exists='replace', index=False, index_label='id')
        print("Finished process COIN: {}".format(coin))

    except Exception as error:

        if str(error) == "'NoneType' object has no attribute 'findChildren'":

            print(error)

            get_price_marketcap(coin, '20130428')
        else:

            print(error)

            # Create handlers
            f_handler = logging.FileHandler('save_coin_marketcap_information_error.log')

            # Create formatters and add it to handlers
            f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            f_handler.setFormatter(f_format)

            # Add handlers to the logger
            logger.addHandler(f_handler)
            logger.error('COIN: %s ERROR %s', coin, error)



if __name__ == "__main__":

    core('19900101')
