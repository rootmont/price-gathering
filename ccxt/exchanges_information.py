import ccxt
import pandas as pd
import logging
from threading import Thread, Lock
from engine import mydb
from common.config import maybe_parallel

list = []

threads = []


output = Lock()

# print("exchange_id: {}, Error: {}".format(exchange_id, error))

# Create a custom logger
logger = logging.getLogger(__name__)

# Create handlers
f_handler = logging.FileHandler('file_name_exchange_get_coin_exchange.log')

# Create formatters and add it to handlers
f_format = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
f_handler.setFormatter(f_format)

# Add handlers to the logger
logger.addHandler(f_handler)


def coin_exchange_into_db():
    listExcep = ["allcoin", "anybits", "bcex", "bibox", "bitsane", "btctradeim",
                 "btcturk", "ccex", "coinegg", "coingi", "cointiger", "coolcoin", "cryptopia",
                 "flowbtc", "liqui", "rightbtc", "stronghold", "theocean", "uex", "xbtce", "bitz"]
    for exchange_id in ccxt.exchanges:
        if exchange_id not in listExcep:
            try:
                t = Thread(target=thread_exchange, args=(exchange_id, list))
                threads.append(t)
            except Exception as e:
                print(e)
                continue
    try:
        for x in threads:
            x.start()
        for x in threads:
            x.join()

        print(list)
        df = pd.DataFrame(list, columns=["name_exchanges"])
        df.to_sql(name="exchange", con=mydb, if_exists='replace', index=False, index_label='id')
    except Exception as e:
        print(e)


def thread_exchange(exchange_id, list):
    try:
        exchange_nema = exchange_id + '_markets_list'
        # print("exchange1 ", exchange_nema)
        list.append(exchange_nema)
        exchange = getattr(ccxt, exchange_id)()
        # print(exchange_id)
        df = pd.DataFrame(exchange.load_markets().keys(), columns=["coin_list"])
        df.to_sql(name=exchange_id + "_markets_list", con=mydb, if_exists='replace', index=False, index_label='id')
    except Exception as error:
        logger.error('EXCHANGE: %s', exchange_id)


def get_coin_exchange_with_params(exchange, coin):
    
    try:

        print("Starting - Getting Data for exchange: {} coin:{}".format(exchange, coin))

        exchange_ = getattr(ccxt, exchange)()

        if exchange == 'gdax':

            from control_gdax_exchange import get_data_by_market_gdax

            get_data_by_market_gdax(exchange, coin)

        elif exchange == 'hitbtc':

            from control_poloniex_exchange import get_data_by_market_polonix

            exchange == 'poloniex'

            get_data_by_market_polonix(coin, exchange)

        elif exchange == 'hitbtc':

            from control_hitbtc_exchange import get_data_by_market_hitbtc

            exchange == 'hitbtc2'
            get_data_by_market_hitbtc(exchange_, coin, exchange)

        else:

            from exchanges_control import get_data_by_market

            get_data_by_market(exchange_, coin, exchange)

        print("Completed - Getting Data for exchange: {} coin:{}".format(exchange, coin))

    except Exception as error:

        #logging.error('%s ***exchange: %s ***coin %s ***error ', exchange, coin, error)

        logger.error('%s ***exchange: %s ***coin %s ***error' % (exchange, coin, error))
