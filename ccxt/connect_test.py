import pymysql.cursors
import pandas as pd
from sqlalchemy import create_engine
from credentials_db import user, passw, host, port, database

list = []

user = user
passw = passw
host = host
port = port
database = database

mydb = create_engine('mysql+pymysql://' + user + ':' + passw + '@' + host + ':' + str(port) + '/' + database, echo=False)

# Connect to the database
connection = pymysql.connect(host=host,
                             port=port,
                             user=user,
                             password=passw,
                             db=database,
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

try:
    with connection.cursor() as cursor:
        sql = "SELECT table_name " \
              "FROM information_schema.tables " \
              "WHERE table_schema = 'test_exchanges_coins' " \
              "and table_name like '%_markets_list'"
        cursor.execute(sql)
        result = cursor.fetchall()
        for i in result:
            for a in i.values():
                list.append(a)

finally:
    connection.close()


def get_coins_db_gdax():

    from exchanges_information import get_coin_exchange_with_params

    for exchage in list:

        print("i", exchage)

        if exchage != 'mandala_markets_list':

            df_exchanges_coins = pd.read_sql_table(exchage, mydb)

            df_exchanges_coins_list = df_exchanges_coins.values.tolist()

            print("coin list ", df_exchanges_coins_list)

            for market in df_exchanges_coins_list[:1]:

                exchange_name = exchage.replace('_markets_list','')
                print(exchange_name)

                market_name = market[0]
                print(market_name)

                # print("EXCHANGE: {}, MARKET: {}".format(exchange_name, market_name))

                # print("-- GETTING DATA EXCHANGE: {}, MARKET: {}".format(exchange_name, market_name))

                get_coin_exchange_with_params(exchange_name, market_name)

                # print("-- FINISHING EXCHANGE: {}, MARKET: {}".format(exchange_name, market_name))

        else:
            print(exchage)




get_coins_db_gdax()