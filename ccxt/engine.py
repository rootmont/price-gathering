from credentials_db import user, passw, host, port, database
from sqlalchemy import create_engine

url = 'mysql+pymysql://' + user + ':' + passw + '@' + host + ':' + str(port) + '/' + database
mydb = create_engine(url, echo=False, pool_size=20, max_overflow=0)


