import ccxt
import pandas as pd
from sqlalchemy import create_engine
import logging
from credentials_db import user, passw, host, port, database
from threading import Thread, Lock
from get_coins_db import get_coins_db

user = user
passw = passw
host = host
port = port
database = database

list = []

threads = []

engine = create_engine('mysql+pymysql://' + user + ':' + passw + '@' + host + ':' + str(port) + '/' + database, echo=False, pool_size=20, max_overflow=0)

#cnn = engine.connect()
cnn = engine

output = Lock()

# Create a custom logger
logger = logging.getLogger(__name__)

# Create handlers
f_handler = logging.FileHandler('file_name_exchange_get_coin_exchange.log')

# Create formatters and add it to handlers
f_format = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
f_handler.setFormatter(f_format)

# Add handlers to the logger
logger.addHandler(f_handler)


def coin_exchange_into_db():
    print("--------- --------- --------- Starting Getting coins exchanges information... --------- --------- ---------")

    listExcep = ["allcoin", "anybits", "bcex", "bibox", "bitsane", "btctradeim",
                 "btcturk", "ccex", "coinegg", "coingi", "cointiger", "coolcoin", "cryptopia",
                 "flowbtc", "liqui", "rightbtc", "stronghold", "theocean", "uex", "xbtce", "bitz"]

    counter = 1
    total = len(ccxt.exchanges)
    for exchange_id in ccxt.exchanges:
        if exchange_id not in listExcep:
            print("Getting coins from exchange: {} - {}/{}".format(exchange_id, counter, total))
            counter = counter + 1
            try:
                t = Thread(target=thread_exchange, args=(exchange_id, list))
                threads.append(t)
            except Exception as e:
                print("Error: Getting coins from exchange: {} - {}/{} - ERROR: {}".format(exchange_id, counter, total, e))
                continue
    try:
        for x in threads:
            x.start()
        for x in threads:
            x.join()

        df = pd.DataFrame(list, columns=["name_exchanges"])
        df.to_sql(name="exchange", con=cnn, if_exists='replace', index=False, index_label='id')

        print("--------- --------- --------- Finished Getting coins exchanges information... --------- --------- ---------")

    except Exception as e:
        print(e)


def thread_exchange(exchange_id, list):

    try:

        exchange_nema = exchange_id + '_markets_list'

        list.append(exchange_nema)

        exchange = getattr(ccxt, exchange_id)()

        df = pd.DataFrame(exchange.load_markets().keys(), columns=["coin_list"])

        df.to_sql(name=exchange_id + "_markets_list", con=cnn, if_exists='replace', index=False, index_label='id')
    
    except Exception as error:

        print("Error: Thread Exchange:{} ERROR:{}".format(exchange_id,error))


def get_coin_exchange_with_params(exchange, coin):
    try:

        print(exchange)

        exchange_ = getattr(ccxt, exchange)()

        print(exchange_)

        if exchange == 'gdax':

            from control_gdax_exchange import get_data_by_market_gdax

            dg = get_data_by_market_gdax(exchange, coin)

        elif exchange == 'hitbtc':

            from control_poloniex_exchange import get_data_by_market_polonix

            exchange == 'poloniex'

            dp = get_data_by_market_polonix(coin, exchange)

            print(exchange)

        elif exchange == 'hitbtc':

            from control_hitbtc_exchange import get_data_by_market_hitbtc

            exchange == 'hitbtc2'
            dh = get_data_by_market_hitbtc(exchange_, coin, exchange)

            print(exchange)

        else:
            from exchanges_control import get_data_by_market

            df = get_data_by_market(exchange_, coin, exchange)

    except Exception as error:

        logging.error('%s ***exchange: %s ***coin %s ***error ', exchange, coin, error)



if __name__ == "__main__":

    #coin_exchange_into_db()

    get_coins_db()