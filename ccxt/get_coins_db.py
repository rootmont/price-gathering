import pandas as pd
import ccxt
# import mysql
import time
from sqlalchemy import create_engine
from credentials_db import user, passw, host, port, database
from threading import Thread, Lock
from common.config import maybe_parallel, format_logger
import random
from exchanges_information import get_coin_exchange_with_params
import logging

user = user
passw = passw
host = host
port = port
database = database

threads = []

output = Lock()

mysql_url = 'mysql+pymysql://' + user + ':' + passw + '@' + host + ':' + str(port) + '/' + database
mydb = create_engine(mysql_url, echo=False, pool_size=30, max_overflow=0, connect_args={'connect_timeout': 30})

logger = logging.getLogger('get_coins_db')
format_logger(logger)

# array_exchanres_error = []

def get_coins_db():

    try:

        print("--------- --------- --------- Starting Getting DATA for coins by exchange information... --------- --------- ---------")

        query = 'SELECT table_name FROM information_schema.tables WHERE table_schema = {} and table_name like {}'.format("'ccxt'","'%%_markets_list%%'")

        df = pd.read_sql_query(query, mydb)

        df_exchanges_list = df.values.tolist()

        print("ORIGINAL LIST: {}".format(df_exchanges_list))

        print("FILTERED LIST: {}".format(df_exchanges_list))

        table_name = 'ccxt_coins'

        df = pd.DataFrame([], columns=["timestamp", "open", "high", "low", "close", "volume", "symbol", "exchange"])

        df.to_sql(name=table_name, con=mydb, if_exists='append', index=False, index_label=['id'])

        random.shuffle(df_exchanges_list)

        counter_batch = 1

        maybe_parallel(fxn=get_exchange, args=df_exchanges_list, parallel=False, num_processes=10)

        # while df_exchanges_list:
        #
        #     threads = []
        #
        #     for i in df_exchanges_list[:9]:
        #
        #         try:
        #
        #             t = Thread(target=thread_one, args=(i,))
        #
        #             threads.append(t)
        #
        #             #thread_one(i)
        #
        #             print("i:{}-{}".format(i, counter_batch))
        #
        #             counter_batch = counter_batch + 1
        #
        #         except Exception as e:
        #
        #             print(e)
        #
        #     for x in threads:
        #         x.start()
        #
        #     df_exchanges_list = df_exchanges_list[10:]
        #
        # while True:
        #
        #     time.sleep(5)
        #
        #     print("Running...")

    except Exception as error:

        logger.error(error)
    

 
    print("--------- --------- --------- Finishing Getting DATA for coins by exchange information... --------- --------- ---------")


def get_exchange(exchange):
    # mydb = create_engine(mysql_url, echo=False, pool_size=30, max_overflow=0)

    df_exchanges_coins = pd.read_sql_table(exchange[0], mydb)

    df_exchanges_coins_list = df_exchanges_coins.values.tolist()

    for market in df_exchanges_coins_list[:1]:

        exchange_name = exchange[0].replace('_markets_list', '')

        market_name = market[0]

        # print("EXCHANGE: {}, MARKET: {}".format(exchange_name, market_name))

        # print("-- GETTING DATA EXCHANGE: {}, MARKET: {}".format(exchange_name, market_name))

        get_coin_exchange_with_params(exchange_name, market_name)

        # print("-- FINISHING EXCHANGE: {}, MARKET: {}".format(exchange_name, market_name))


# def thread_one(i):
#
#     from exchanges_information import get_coin_exchange_with_params
#
#     df_exchanges_coins = pd.read_sql_table(i[0], mydb)
#
#     df_exchanges_coins_list = df_exchanges_coins.values.tolist()
#
#     for market in df_exchanges_coins_list[:1]:
#
#         exchange_name = i[0].replace('_markets_list', '')
#
#         market_name = market[0]
#
#         # print("EXCHANGE: {}, MARKET: {}".format(exchange_name, market_name))
#
#         # print("-- GETTING DATA EXCHANGE: {}, MARKET: {}".format(exchange_name, market_name))
#
#         get_coin_exchange_with_params(exchange_name, market_name)
#
#         # print("-- FINISHING EXCHANGE: {}, MARKET: {}".format(exchange_name, market_name))


def get_coins_db_gdax():
    from exchanges_information import get_coin_exchange_with_params

    query2 = """
            SELECT * FROM coin_exchange_db.gdax_markets_list;
        """.format(database)

    df = pd.read_sql_query(query2, mydb)

    df_exchanges_list = df.values.tolist()

    print(df_exchanges_list)

    for coin in df_exchanges_list:

        # df_exchanges_coins = pd.r
        # ead_sql_table(i[0], mydb)

        # df_exchanges_coins_list = df_exchanges_coins.values.tolist()

        print("coin list ", coin)

        for market in coin[:1]:
            
            exchange_name = 'gdax'

            print("exchange_name", exchange_name)

            market_name = market

            print("market_name", market_name)

            # print("EXCHANGE: {}, MARKET: {}".format(exchange_name, market_name))

            # print("-- GETTING DATA EXCHANGE: {}, MARKET: {}".format(exchange_name, market_name))

            get_coin_exchange_with_params(exchange_name, market_name)

            # print("-- FINISHING EXCHANGE: {}, MARKET: {}".format(exchange_name, market_name))
