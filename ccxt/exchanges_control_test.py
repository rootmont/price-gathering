import pandas as pd
import ccxt
from sqlalchemy import create_engine
import datetime
from datetime import datetime as dt
import traceback
from pprint import pprint
import logging
import gdax
from credentials_db import user, passw, host, port, database


user = user
passw = passw
host = host
port = port
database = database

mydb = create_engine('mysql+pymysql://' + user + ':' + passw + '@' + host + ':' + str(port) + '/' + database,
                     echo=False)

epoch_now = datetime.datetime.now().timestamp() * 1000


def get_initDate_endDate():
    client = ccxt.gdax({'enableRateLimit': True})
    start_point_time = '2000-11-08T00:00:00'
    actual = epoch_now
    listI = []
    start_point_time_ = client.parse8601(start_point_time)
    try:
        next_date_start = start_point_time_
        next_date_start = client.iso8601(start_point_time_)
        epoch_next_date_start = 0
        flag = True
        if client.id == 'gdax':
            while epoch_next_date_start < actual and flag is True:
                publicClient = gdax.PublicClient()
                date_time_str = '2000-11-08 00:00:00.000000'
                date_time_obj = datetime.datetime.strptime(date_time_str, '%Y-%m-%d %H:%M:%S.%f')
                print("date_time_obj", date_time_obj)
                print(date_time_obj.isoformat())
                hist = publicClient.get_product_historic_rates(
                    "BTC-EUR",
                    start=date_time_obj.isoformat(),
                    granularity=3600)
                var = date_time_obj.isoformat()
                print("var", var)
                date_time_obj_ = client.parse8601(var)
                print("date_time_obj_", date_time_obj_)
                listI = listI + hist
                if date_time_obj_ == client.iso8601(hist[-1][0]):
                    print("en el if ", hist[-1][0])
                    flag = False
                else:
                    date_time_obj_ = client.iso8601(hist[-1][0])
                    print("en el else", client.iso8601(hist[-1][0]))
                    print("en el else hist", hist[-1][0])
                    print("next_date_start ", next_date_start)
                print(listI)
        else:
            while epoch_next_date_start < actual and flag is True:
                ohlcv = client.fetch_ohlcv('BTC/USD', '1h', client.parse8601(next_date_start), 750)
                listI = listI + ohlcv
                if next_date_start == client.iso8601(ohlcv[-1][0]):
                    print("next_date_start ", next_date_start)
                    print("en el if ", client.iso8601(ohlcv[-1][0]))
                    flag = False
                else:
                    next_date_start = client.iso8601(ohlcv[-1][0])
                    print("next_date_start ", next_date_start)
                    print("en el else", client.iso8601(ohlcv[-1][0]))
            print(listI)

    except Exception as error:
        # Create a custom logger
        logger = logging.getLogger(__name__)

        # print("EXCHANGE: {}, ERROR: {}".format(exchange_name, error))

        # Create handlers
        f_handler = logging.FileHandler('file4.log')

        # Create formatters and add it to handlers
        f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        f_handler.setFormatter(f_format)

        # Add handlers to the logger
        logger.addHandler(f_handler)
        #logger.error('EXCHANGE: %s ERROR %s', exchange_name, error)
        logging.error('EXCHANGE: %s', error)

        # print("COIN: {}, Error: {}".format(coin, error))
        # print(traceback.format_exc())

        pass

#get_initDate_endDate()

mandala = ccxt.hitbtc
print(mandala.load_markets().keys())