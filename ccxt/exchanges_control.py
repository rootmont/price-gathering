import pandas as pd
import ccxt
from sqlalchemy import create_engine
import datetime
from datetime import datetime as dt
import traceback
from pprint import pprint
import logging
import pymysql
from credentials_db import user, passw, host, port, database
from common.config import maybe_parallel, format_logger


user = user
passw = passw
host = host
port = port
database = database

mydb = create_engine('mysql+pymysql://' + user + ':' + passw + '@' + host + ':' + str(port) + '/' + database, echo=False, pool_size=20, max_overflow=0)

epoch_now = datetime.datetime.now().timestamp()*1000

array_exchanres_error = []
dict = {}

logger = logging.getLogger('get_coins_db')
format_logger(logger)

# array_exchanres_error = []


def get_data_by_market(client, coin, exchange_name):

    logger.info("Starting thread, Data for exchange: {} coin: {}".format(exchange_name, coin))

    start_point_time = '2000-11-08T00:00:00'

    actual = epoch_now

    #print("COIN: {}".format(coin))
    #print("EXCHANGE NAME: {}".format(exchange_name))

    listI = []

    #global array_exchanres_error

    global array_exchanres_error
    global dict

    start_point_time_ = client.parse8601(start_point_time)

    try:

        next_date_start = client.iso8601(start_point_time_)

        epoch_next_date_start = 0

        #print(start_point_time_)

        flag = True

        while epoch_next_date_start < actual and flag is True:

            ohlcv = client.fetch_ohlcv(coin, timeframe='1h', since=client.parse8601(next_date_start), limit=10000)

            #print("-- Getting data COIN:{} for actual: {} start_date: {} next_formatted_date: {} elements: {} total_in_list {}".format(coin, actual, start_point_time_, next_date_start, len(ohlcv), len(listI)))

            listI = listI + ohlcv

            df_temp = pd.DataFrame(listI, columns=["timestamp", "open", "high", "low", "close", "volume"])

            df_temp["timestamp"] = pd.to_datetime(df_temp["timestamp"], unit='ms')

            logger.debug(df_temp)

            date_start_ = next_date_start

            if next_date_start == client.iso8601(ohlcv[-1][0]):
                
                flag = False

            else:

                next_date_start = client.iso8601(ohlcv[-1][0])

            #print(next_date_start)

            epoch_next_date_start = ohlcv[-1][0]
            
            logger.info("Getting data Exchange:{} COIN:{} for start_date: {} next_formatted_date: {} elements: {} total_in_list {}".format(exchange_name, coin, date_start_, next_date_start, len(ohlcv), len(listI)))
            #print("I: {}".format(ohlcv))

        df = pd.DataFrame(listI, columns=["timestamp", "open", "high", "low", "close", "volume"])

        df["symbol"] = coin

        df["exchange"] = exchange_name

        table_name = '{}_{}'.format(exchange_name, coin)

        df.to_sql(name=table_name, con=mydb, if_exists='replace', index=False, index_label=['id'])

        num_retries=5
        for i in range(num_retries):
            try:
                with mydb.begin() as cn:
                    sql = (
                        "INSERT INTO ccxt.{0} (timestamp, open, high, low, close, volume, symbol, exchange) "
                        "SELECT t.timestamp, t.open, t.high, low, t.close, t.volume, t.symbol, t.exchange "
                        "FROM ccxt.`{1}` t "
                        "WHERE NOT EXISTS "
                            "(SELECT 1 FROM ccxt.{0} f "
                            "WHERE t.symbol = f.symbol "
                            "AND t.exchange = f.exchange "
                            "AND t.timestamp = f.timestamp)"
                    ).format("`ccxt_coins`", table_name)
                    logger.debug(sql)
                    cn.execute(sql)
                    break
            except pymysql.err.InternalError as error:
                logger.warning(error)
                if i < num_retries - 1:
                    logger.warning('Restarting transaction')
                else:
                    logger.error('ERROR REPEATED {} TIMES'.format(num_retries))

        logger.info("Thread DATA, Saved DATA {}, exchange: {} coin: {}".format(len(df),exchange_name, coin))


    except Exception as error:

        #print("Thread DATA, exchange: {} coin: {}, Error: {}".format(exchange_name, coin, error))

        logger.error("Thread DATA, exchange: {} coin: {}, Error {}".format(exchange_name, coin, str(error)[:75]))

