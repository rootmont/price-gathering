from app.flask_factory import db

from sqlalchemy import Index

from sqlalchemy.schema import FetchedValue

# coding: utf-8
from sqlalchemy import Column, DateTime, ForeignKey, Integer, Numeric, SmallInteger, String, Text
from sqlalchemy.dialects.mysql import INTEGER, TINYINT, LONGTEXT, SMALLINT, TIMESTAMP, BIGINT

from sqlalchemy.schema import FetchedValue
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

from app.models.mixins import TimeStampsMixin, SoftDeletesMixin

from datetime import datetime, timedelta

Base = declarative_base()
metadata = Base.metadata


def default_coin1_markets(context):
    print(context.get_current_parameters())
    return str(context.get_current_parameters()['symbol']).split('/')[0]

def default_coin2_markets(context):
    print(context.get_current_parameters())
    return str(context.get_current_parameters()['symbol']).split('/')[1]
    
def default_coin1_information(context):
    return str(context.get_current_parameters()['coin']).split('/')[0]

def default_coin2_information(context):
    return str(context.get_current_parameters()['coin']).split('/')[1]
class CCXTHourlyInformation(db.Model, TimeStampsMixin, SoftDeletesMixin):
    __tablename__ = 'coins_hourly_information'


    id = db.Column(db.Integer, primary_key=True)
    exchange = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    coin = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    coin1 = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True, default= default_coin1_information)
    coin2 = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True, default= default_coin2_information)
    open = db.Column(db.Float('20, 14'), nullable=True)
    high = db.Column(db.Float('20, 14'), nullable=True)
    low = db.Column(db.Float('20, 14'), nullable=True)
    close = db.Column(db.Float('20, 14'), nullable=True)
    volume = db.Column(db.Float('20, 14'), nullable=True)
    timestamp = db.Column(db.DateTime)

class CCXTMarkets(db.Model, TimeStampsMixin, SoftDeletesMixin):
    __tablename__ = 'markets'

    id = db.Column(db.Integer, primary_key=True)
    exchange = db.Column(db.String(191, 'utf8mb4_unicode_ci'), index=True)
    symbol = db.Column(db.String(191, 'utf8mb4_unicode_ci'), index=True)
    coin1 = db.Column(db.String(191, 'utf8mb4_unicode_ci'), index=True, default= default_coin1_markets)
    coin2 = db.Column(db.String(191, 'utf8mb4_unicode_ci'), index=True, default= default_coin2_markets)
    inprogress =  db.Column(db.Boolean, default=False)
    completed = db.Column(db.Boolean, default=False)
    error = db.Column(db.Boolean, default=False)
    details_error = db.Column(LONGTEXT, default='')
    incremental_inprogress = db.Column(db.Boolean, default=False)
    incremental_completed = db.Column(db.Boolean, default=False)
    incremental_error = db.Column(db.Boolean, default=False)
    incremental_details_error = db.Column(LONGTEXT, default='')