# coding: utf-8

import logging
import json
import time
import datetime


from flask_restplus import Resource
from app.rest.api import api
from app.rest.ccxt.serializers import dummy_serializer
from app.ccxt.ccxt import ccxtModule

log = logging.getLogger(__name__)
ns = api.namespace('ccxt', description='Pulling and gathering system for CCXT')


@ns.route('/')
class CCXTService(Resource):

    @api.expect(dummy_serializer, validate=True)
    def post(self):
        ccxt_instance = ccxtModule()
        return ccxt_instance.get_coins_information_incremental(**api.payload)



@ns.route('/run/markets')
class CCXTRunMarkets(Resource):

    def get(self):
        ccxt_instance = ccxtModule()
        return ccxt_instance.iterate_coins_in_markets()

@ns.route('/run/markets/incremental')
class CCXTRunMarketsIncremental(Resource):

    def get(self):
        ccxt_instance = ccxtModule()
        return ccxt_instance.iterate_coins_in_markets_incremental()

@ns.route('/load/markets')
class CCXTLoadMarkets(Resource):

    def get(self):
        ccxt_instance = ccxtModule()
        return ccxt_instance.load_markets()

@ns.route('/reset/markets')
class CCXTResetMarkets(Resource):

    def get(self):
        ccxt_instance = ccxtModule()
        return ccxt_instance.reset_market()
