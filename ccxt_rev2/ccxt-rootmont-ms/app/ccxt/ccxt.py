from app.models.models import (CCXTHourlyInformation,
								CCXTMarkets)

from sqlalchemy import and_, func
from app.flask_factory import db
import pandas as pd
import ccxt
import datetime
from datetime import datetime as dt
from datetime import datetime as dat
import traceback
from pprint import pprint
import logging
from common.config import format_logger

logger = logging.getLogger('get_coins_db')
format_logger(logger)


class Timer:
    """
        Tracker using time
    """
    def __init__(self):
        self.start = dat.now()
 
    def log(self, desc=None):
        difference = dat.now() - self.start
        if(desc):
            logger.info("** {} - {}".format(difference, desc))
        else:
            logger.info("** {}".format(difference))
 
    def stop(self):
        self.log(desc="Finished")
        self.start = dat.now()
 
    def reset_timer(self):
        self.start = dat.now()

class ccxtModule:

	def __init__(self):

		self.epoch_now = datetime.datetime.now().timestamp()*1000
	
	def get_data_by_market(self, client, coin, exchange_name, epoch_now):

		logger.info("Starting thread, Data for exchange: {} coin: {}".format(exchange_name, coin))

		start_point_time = '2000-11-08T00:00:00'

		actual = epoch_now

		#print("COIN: {}".format(coin))
		#print("EXCHANGE NAME: {}".format(exchange_name))

		listI = []

		start_point_time_ = client.parse8601(start_point_time)

		try:

			next_date_start = client.iso8601(start_point_time_)

			epoch_next_date_start = 0

			flag = True

			while epoch_next_date_start < actual and flag is True:

				ohlcv = client.fetch_ohlcv(coin, timeframe='1h', since=client.parse8601(next_date_start), limit=10000)

				#print("-- Getting data COIN:{} for actual: {} start_date: {} next_formatted_date: {} elements: {} total_in_list {}".format(coin, actual, start_point_time_, next_date_start, len(ohlcv), len(listI)))

				listI = listI + ohlcv

				df_temp = pd.DataFrame(listI, columns=["timestamp", "open", "high", "low", "close", "volume"])

				df_temp["timestamp"] = pd.to_datetime(df_temp["timestamp"], unit='ms')

				logger.debug(df_temp)

				date_start_ = next_date_start

				if next_date_start == client.iso8601(ohlcv[-1][0]):
					
					flag = False

				else:

					next_date_start = client.iso8601(ohlcv[-1][0])

				#print(next_date_start)

				epoch_next_date_start = ohlcv[-1][0]
				
				logger.info("Getting data Exchange:{} COIN:{} for start_date: {} next_formatted_date: {} elements: {} total_in_list {}".format(exchange_name, coin, date_start_, next_date_start, len(ohlcv), len(listI)))
				#print("I: {}".format(ohlcv))

			df = pd.DataFrame(listI, columns=["timestamp", "open", "high", "low", "close", "volume"])

			#df = df.head(100)

			df["symbol"] = coin

			df["exchange"] = exchange_name

			table_name = '{}_{}'.format(exchange_name, coin)

			#df["timestamp"] = pd.to_datetime(df["timestamp"],unit='ms')

			#df["timestamp"] = df["timestamp"]

			#print(df["timestamp"])

			#df.to_sql(name=table_name, con=mydb, if_exists='replace', index=False, index_label=['id'])

			logger.info("Thread DATA, Saved DATA {}, exchange: {} coin: {}".format(len(df),exchange_name, coin))

			return df

		except Exception as error:

			#print("Thread DATA, exchange: {} coin: {}, Error: {}".format(exchange_name, coin, error))

			market_db_element = CCXTMarkets.query.filter_by(exchange=exchange_name, symbol=coin).all()[0]

			market_db_element.error = True

			market_db_element.completed = False

			market_db_element.inprogress = False

			db.session.commit()

			logger.error("Thread DATA, exchange: {} coin: {}, Error {}".format(exchange_name, coin, str(error)[:75]))


	def get_data_by_market_24h_incremental(self, client, coin, exchange_name, epoch_now, hours=24):

		listI = []

		logger.info("Starting thread, Data for exchange: {} coin: {}".format(exchange_name, coin))

		try:


			ohlcv = client.fetch_ohlcv(coin, timeframe='1h', limit=24)

			listI = listI + ohlcv

			df_temp = pd.DataFrame(listI, columns=["timestamp", "open", "high", "low", "close", "volume"])

			df_temp["timestamp"] = pd.to_datetime(df_temp["timestamp"], unit='ms')

			logger.debug('Dataframe: ')

			logger.debug(df_temp)
			
			logger.debug('Raw: ')

			logger.debug(listI)

			df = pd.DataFrame(listI, columns=["timestamp", "open", "high", "low", "close", "volume"])

			#df = df.head(100)

			df["symbol"] = coin

			df["exchange"] = exchange_name

			logger.info("Thread DATA, Saved DATA {}, exchange: {} coin: {}".format(len(df),exchange_name, coin))

			return df

		except Exception as error:

			#print("Thread DATA, exchange: {} coin: {}, Error: {}".format(exchange_name, coin, error))

			market_db_element = CCXTMarkets.query.filter_by(exchange=exchange_name, symbol=coin).first()
			
			print(market_db_element)

			market_db_element.incremental_error = True

			market_db_element.incremental_completed = False

			market_db_element.incremental_inprogress = False

			market_db_element.incremental_details_error = str(error)

			db.session.commit()

			logger.error("Thread DATA, exchange: {} coin: {}, Error {}".format(exchange_name, coin, str(error)[:75]))



	
	def save_database(self, df):

		import time
		import json

		response_time = Timer()

		logger.info("API is starting")

		payload = {"result":True}

		try:

			#df = df.head(50)
            
			output = df.to_dict('records')

			added_counter = 0

			for index, item in enumerate(output):

				item["timestamp"] = dat.utcfromtimestamp(item["timestamp"]/1000)

				preview = db.query(CCXTHourlyInformation).filter(CCXTHourlyInformation.coin == item["symbol"],\
					CCXTHourlyInformation.exchange == item["exchange"],\
					CCXTHourlyInformation.timestamp == item["timestamp"]).all()
				
				if preview:

					logger.info("Duplicated element: {}".format(preview))
					
					continue

				logger.info("TICKER: {} EXCHANGE: {} IN PERIOD: {}".format(item["symbol"], item["exchange"], item["timestamp"]))

				ccxt_object =  CCXTHourlyInformation(
					timestamp = item["timestamp"], 
					coin = item["symbol"],
					exchange = item["exchange"],
					open = item["open"],
					high = item["high"],
					low = item["low"],
					close = item["close"],
					volume = item["volume"]
                )

				db.session.merge(ccxt_object)
				added_counter = added_counter + 1
            
			db.session.commit()

			market_db_element = CCXTMarkets.query.filter_by(exchange=item["exchange"], symbol=item["symbol"]).first()

			market_db_element.inprogress = False

			market_db_element.completed = True

			db.session.commit()

			#payload["data"] = item

			response_time_delta = dat.now() - response_time.start

			payload["stats"] = {"total_elements":added_counter, "response_time":response_time_delta.total_seconds()}

			return payload
                
		except Exception as e:

			logger.info("Exception when calling cctx: %s\n" % e)

			payload = {"result":False}

			payload["reason"] = "API-Exception, please check system logs"
            
			time.sleep(5)

			return payload

	def save_database_incremental(self, df):

		import time
		import json

		response_time = Timer()

		logger.info("API is starting")

		payload = {"result":True}

		try:

			#df = df.head(50)
            
			output = df.to_dict('records')

			added_counter = 0

			for index, item in enumerate(output):

				item["timestamp"] = dat.utcfromtimestamp(item["timestamp"]/1000)

				preview = CCXTHourlyInformation.query.filter_by(coin=item["symbol"], exchange = item["exchange"], timestamp = item["timestamp"]).all()
				
				if preview:

					logger.info("Duplicated element: {}".format(preview))
					
					continue

				logger.info("TICKER: {} EXCHANGE: {} IN PERIOD: {}".format(item["symbol"], item["exchange"], item["timestamp"]))

				ccxt_object =  CCXTHourlyInformation(
					timestamp = item["timestamp"], 
					coin = item["symbol"],
					exchange = item["exchange"],
					open = item["open"],
					high = item["high"],
					low = item["low"],
					close = item["close"],
					volume = item["volume"]
                )

				db.session.merge(ccxt_object)
				added_counter = added_counter + 1
            
			db.session.commit()

			market_db_element = CCXTMarkets.query.filter_by(exchange=item["exchange"], symbol=item["symbol"]).all()[0]

			market_db_element.incremental_inprogress = False

			market_db_element.incremental_completed = True

			db.session.commit()

			#payload["data"] = item

			response_time_delta = dat.now() - response_time.start

			payload["stats"] = {"total_elements":added_counter, "response_time":response_time_delta.total_seconds()}

			return payload
                
		except Exception as e:

			logger.info("Exception when calling cctx: %s\n" % e)

			payload = {"result":False}

			payload["reason"] = "API-Exception, please check system logs"
            
			time.sleep(5)

			return payload

	def save_market_to_database(self, df, exchange):

		import time
		import json

		response_time = Timer()

		logger.info("API is starting")

		payload = {"result":True}

		try:
            
			output = df.to_dict('records')

			added_counter = 0

			for index, item in enumerate(output):

				print(item)

				preview = CCXTMarkets.query.filter_by(symbol=item["coin_list"], exchange = exchange).all()
				
				if preview:

					logger.info("Duplicated element: {}".format(preview))
					
					continue

				logger.info("TICKER: {} EXCHANGE: {}".format(item["coin_list"], exchange))

				ccxt_market_object =  CCXTMarkets(
					symbol = item["coin_list"],
					exchange = exchange,
					inprogress = False,
					completed = False,
					error = False
                )

				db.session.merge(ccxt_market_object)
				added_counter = added_counter + 1
            
			db.session.commit()

			#payload["data"] = item

			response_time_delta = dat.now() - response_time.start

			payload["stats"] = {"total_elements":added_counter, "response_time":response_time_delta.total_seconds()}

			return payload
                
		except Exception as e:

			logger.info("Exception when calling cctx markets: %s\n" % e)

			payload = {"result":False}

			payload["reason"] = "API-Exception, please check system logs"
            
			time.sleep(5)

			return payload



	def get_coin_exchange_with_params(self, exchange, coin):
		
		try:

			logger.info("Starting - Getting Data for exchange: {} coin:{}".format(exchange, coin))

			exchange_ = getattr(ccxt, exchange)()

			data = self.get_data_by_market(exchange_, coin, exchange, self.epoch_now)

			results = self.save_database(data)

			logger.info("Completed - Getting Data for exchange: {} coin:{}".format(exchange, coin))

			return results

		except Exception as error:

			#logging.error('%s ***exchange: %s ***coin %s ***error ', exchange, coin, error)

			logger.error('%s ***exchange: %s ***coin %s ***error' % (exchange, coin, error))

	def get_coin_exchange_with_params_incremental(self, exchange, coin):
		

		logger.info("Starting - Getting Data for exchange: {} coin:{}".format(exchange, coin))

		exchange_ = getattr(ccxt, exchange)()

		data = self.get_data_by_market_24h_incremental(exchange_, coin, exchange, self.epoch_now)

		results = self.save_database_incremental(data)

		logger.info("Completed - Getting Data for exchange: {} coin:{}".format(exchange, coin))

		return results


	def get_coins_information(self, exchange, coin):

		coin_information = self.get_coin_exchange_with_params(exchange, coin)

		return coin_information

	def get_coins_information_incremental(self, exchange, coin):

		coin_information = self.get_coin_exchange_with_params_incremental(exchange, coin)

		return coin_information

	def thread_exchange(self, exchange_id, list):

		try:

			exchange = getattr(ccxt, exchange_id)()

			df = pd.DataFrame(exchange.load_markets().keys(), columns=["coin_list"])

			self.save_market_to_database(df, exchange_id)
		
		except Exception as error:

			logger.error("Error: Thread Exchange:{} ERROR:{}".format(exchange_id,error))


	def load_markets(self):

		logger.info("--------- --------- --------- Starting Getting coins exchanges information... --------- --------- ---------")

		listExcep = ["allcoin", "anybits", "bcex", "bibox", "bitsane", "btctradeim",
					"btcturk", "ccex", "coinegg", "coingi", "cointiger", "coolcoin", "cryptopia",
					"flowbtc", "liqui", "rightbtc", "stronghold", "theocean", "uex", "xbtce", "bitz"]

		try:

			counter = 1

			total = len(ccxt.exchanges)

			for exchange_id in ccxt.exchanges:

				if exchange_id not in listExcep:

					logger.info("Getting coins from exchange: {} - {}/{}".format(exchange_id, counter, total))

					counter = counter + 1

					self.thread_exchange(exchange_id, list)

			logger.info("--------- --------- --------- Finished Getting coins exchanges information... --------- --------- ---------")

		except Exception as e:
			
			logger.error(e)


	def iterate_coins_in_markets(self):

		pending_markets = CCXTMarkets.query.filter_by(inprogress=False, completed=False, error=False).all()

		total = len(pending_markets)

		logger.info("Processing - Pending markets amount: {}".format(total))

		for index, market in enumerate(pending_markets):

			symbol = market.symbol

			exchange = market.exchange

			logger.info("Processing - Starting exchange:{} and symbol:{}  -  {}/{}".format(symbol, exchange, index, total))

			market_db_element = CCXTMarkets.query.filter_by(exchange=exchange, symbol=symbol).all()[0]

			market_db_element.inprogress = True

			db.session.commit()

			self.get_coins_information(exchange, symbol)

		return True

	def iterate_coins_in_markets_incremental(self):

		pending_markets = CCXTMarkets.query.filter_by(incremental_inprogress=0, incremental_completed=0, incremental_error=0).all()

		total = len(pending_markets)

		logger.info("Processing - Pending markets amount: {}".format(total))

		index = 1

		while pending_markets:

			market = CCXTMarkets.query.filter_by(incremental_inprogress=0, incremental_completed=0, incremental_error=0).first()

			symbol = market.symbol

			exchange = market.exchange

			market.incremental_inprogress = True

			db.session.commit()

			self.get_coins_information_incremental(exchange, symbol)

			index = index + 1
		
		logger.info("Resetting - markets status")

		self.reset_market()

		return True


	def i(self):

		market_db_element = CCXTMarkets.query.all()

		total = len(market_db_element)

		logger.info("Processing - Pending markets amount: {}".format(total))

		for index, market in enumerate(market_db_element):

			symbol = market.symbol

			exchange = market.exchange

			logger.info("Processing - Starting exchange:{} and symbol:{}  -  {}/{}".format(symbol, exchange, index, total))

			market.inprogress = False

			market.completed = False

			market.error = False

			db.session.commit()

		return True


	def reset_market(self):

		market_db_element = CCXTMarkets.query.all()

		total = len(market_db_element)

		logger.info("Processing - Pending markets amount: {}".format(total))

		for index, market in enumerate(market_db_element):

			symbol = market.symbol

			exchange = market.exchange

			logger.info("Processing - Starting exchange:{} and symbol:{}  -  {}/{}".format(symbol, exchange, index, total))

			market.symbol = symbol

			market.inprogress = False

			market.completed = False

			market.error = False

			market.incremental_inprogress = False

			market.incremental_completed = False

			market.incremental_error = False

			market.coin1 = symbol.split('/')[0] if len(symbol.split('/'))>1 else symbol

			market.coin2 = symbol.split('/')[1] if len(symbol.split('/'))>1 else symbol
		
			db.session.merge(market)
            
		db.session.commit()

		return True